<?php /* Template Name: Edubox Landing Page */ ?>

<?php get_header() ?>

<section class="edubox-hero"
     style="background-image: url('<?php echo get_template_directory_uri() . '/img/pattern-background.png' ?>')">
    <div class="container hero-container text-xs-center">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h1 class="hero-title">
                    Pinisi Edubox <br>
                    <small>Ujian Online Mudah dan Lancar Tanpa Kouta</small>
                </h1>
                <p>
                    Melaksanakan ujian online di sekolah dapat menghemat waktu guru dalam proses administrasi seperti pemeriksaan, rekap nilai dan pembuatan rapor. Disamping itu untuk sekolah akan menghasilkan penghematan besar-besaran dalam penggunaan kertas.
                </p>
                <p>
                    Pinisi Edubox siap membantu sekolah anda mewujudkan itu tanpa menggunakan kuota.
                </p>
                <div class="hero-image">
                    <div>
                        <img src="<?php echo get_template_directory_uri() . '/img/edubox-raspi.png' ?>"
                             alt="">
                    </div>
                    <div>
                        <img src="<?php echo get_template_directory_uri() . '/img/edubox-unbk.png' ?>"
                             alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-information">
    <div class="container">
        <div class="row content-padding">
            <div class="col-md-10 offset-md-1 text-xs-center">
                <h2>
                    Satu Alat Cerdas Banyak Fungsi
                </h2>
                <p>
                    Pinisi Edubox memberikan solusi cerdas untuk melaksanakan ujian online di sekolah anda. <br> Dengan Pinisi Edubox tidak ada lagi masalah dengan koneksi internet yang lambat ataupun server down yang dapat menggangu konsentrasi ujian. Guru bisa fokus untuk meningkatkan kualitas pembelajaran, karena semua pekerjaan administratif sudah dikerjakan oleh Pinisi Edubox
                </p>
            </div>
        </div>
        <div class="row content-padding">
            <div class="col-md-4 text-xs-center">
                <img class="img-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/professor.svg' ?>" alt="">
                <h3>
                    Untuk Guru
                </h3>
                <p>
                    Edubox untuk Pengajar dan Guru dapat memudahkan guru untuk menyiapkan dan memeriksa
                    ujian secara otomatis
                </p>
            </div>
            <div class="col-md-4 text-xs-center">
                <img  class="img-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/student.svg' ?>"
                     alt="">
                <h3>
                    Untuk Siswa
                </h3>
                <p>
                    Edubox untuk Siswa dapat memudahkan siswa untuk mengerjakan soal  interaktif
                    dan melihat langsung nilai yang didapat
                </p>
            </div>
            <div class="col-md-4 text-xs-center">
                <img  class="img-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/school.svg' ?>"
                     alt="">
                <h3>
                    Untuk Sekolah
                </h3>
                <p>
                    Edubox untuk sekolah dapat menghemat biaya pengadaan ujian yang sangat besar
                    setiap tahunnya
                </p>
            </div>
        </div>
        <div class="row content-padding">
            <div class="col-md-10 offset-md-1 text-xs-center">
                <h2>
                    Bagaimana Cara Kerjanya?
                </h2>
                <p>
                    Pinisi Edubox memberikan solusi cerdas untuk melaksanakan ujian online di sekolah anda.
                </p>
                <p class="text-center">
                    <iframe width="853" height="480" src="https://www.youtube.com/embed/dN5Xin-_L0I?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                </p>
            </div>
        </div>
</section>

<?php include "partials/section-cta.php";?>

<section class="section-information">
    <div class="container">
        <div class="row content-padding">
            <div class="col-md-12 text-xs-center">
                <h1>Fitur Edubox</h1>
                <p>
                    Aplikasi Edubox berbasis web memudahkan guru dalam pembuatan ujian, upload banyak soal lansung dari Microsoft Word, rekap penilaian proses per-kompetensi dasar dan pembuatan Raport Kurikulum Nasional (Kur 2013)
                </p>
            </div>
        </div>
        <div class="row content-padding features-content text-xs-center">
            <div class="col-md-6">
                <img  class="pn-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/monitor.svg' ?>"
                      alt="">
                <h2>
                    Ujian Berbasis Komputer
                </h2>
                <p>
                    <ul>
                        <li>Mengadakan ulangan harian berbabasis komputer</li>
                        <li>Mengadakan UTS dan UAS berbasis komputer</li>
                        <li>Mengadakan Ujian Sekolah berabasis komputer</li>
                        <li>Rekap nilai untuk pembuatan rapor</li>
                        <li>Siswa bisa langsung mendapatkan hasil ujian</li>
                        <li>Analisis butir soal</li>
                    </ul>
                </p>
            </div>
            <div class="col-md-6">
                <img  class="pn-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/folder.svg' ?>"
                      alt="">
                <h2>
                    Aplikasi Bank Soal dari MS Word
                </h2>
                <p>
                <ul>
                    <li>Guru membuat soal di microsoft word</li>
                    <li>Upload soal secara langsung dari Microsoft Word</li>
                    <li>Edit dan perubahan soal langsung di aplikasi</li>
                    <li>Memuat foto dan equation (Latex dan Microsoft)</li>
                    <li>Memuat audio untuk listening</li>
                </ul>
                </p>
            </div>
        </div>
        <div class="row content-padding features-content text-xs-center">
            <div class="col-md-6">
                <img  class="pn-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/poster.svg' ?>"
                      alt="">
                <h2>
                    Penilaian Proses dan Rapor
                </h2>
                <p>
                <ul>
                    <li>Penilaian proses berbasis kompetensi dasar</li>
                    <li>Rapor angka dan deskripsi otomatis dari sistem</li>
                    <li>Rekap nilai dan leger siswa</li>
                </ul>
                </p>
            </div>
            <div class="col-md-6">
                <img  class="pn-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/idea.svg' ?>"
                      alt="">
                <h2>
                    Materi Pelajaran dan Tugas
                </h2>
                <p>
                    <ul>
                        <li>Guru dapat upload materi pelajaran dokumen dan video</li>
                        <li>Guru bisa membuat tugas untuk dikerjakan dan dikumpulkan siswa berdasarkan waktu yang ditetapkan</li>
                        <li>Buku Sekolah Elektronik (khusus edubox-UNBK smart server)</li>
                    </ul>
                </p>
            </div>
        </div>
        <div class="row content-padding features-content text-xs-center">
            <div class="col-md-6">
                <img  class="pn-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/wikipedia.svg' ?>"
                      alt="">
                <h2>
                    Wikipedia Offline
                </h2>
                <p>
                    <ul>
                        <li>Fitur khusus edubox-UNBK smart server</li>
                        <li>Update secara berkala untuk menyesuaikan dengan fitur online</li>
                        <li>Siswa bisa mencari resource belajar wikipedia tanpa qouta.</li>
                        <li>Bahasa Indonesia dan Inggris</li>
                    </ul>
                </p>
            </div>
            <div class="col-md-6">
                <img  class="pn-icon" src="<?php echo get_template_directory_uri() . '/img/icons/svg/smartphones.svg' ?>"
                      alt="">
                <h2>
                    Fitur Online Offline*
                </h2>
                <p>
                    <ul>
                        <li><i>Tersedia pada pertengahan 2017</i></li>
                        <li>Sinkronisasi data dari aplikasi offline di sekolah</li>
                        <li>Nilai bisa diakses oleh orang tua dari rumah</li>
                        <li>Tugas bisa langsung dikumpulkan dari rumah</li>
                        <li>Guru bisa memberikan ujian dan penugasan dari rumah</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</section>



<section class="section-testimonial">
    <div class="testimonial-container">
        <div class="container">
            <div class="cd-testimonials-wrapper cd-container">
                <h2>Apa Kata Mereka</h2>
                <ul class="cd-testimonials">
                    <li>
                        <div class="testimonial-content">
                            <p>Pinisi Edubox telah memberikan kontribusi dalam memecahkan persoalan teknis mimpi kami dalam mengimplementasikan digital literacy.</p>
                            <div class="cd-author">
                                <img src="http://www.newdesignfile.com/postpic/2012/10/user-account-icon_82574.png" alt="Author image" style="border: 1px solid #eee">
                                <ul class="cd-author-info">
                                    <li>Luqman <strong>Amin</strong><br><small>SMP Darul Hikam</small></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="testimonial-content">
                            <p>Pinisi Edubox telah memberikan kontribusi dalam memecahkan persoalan teknis mimpi kami dalam mengimplementasikan digital literacy</p>
                            <div class="cd-author">
                                <img src="http://www.newdesignfile.com/postpic/2012/10/user-account-icon_82574.png" alt="Author image" style="border: 1px solid #eee">
                                <ul class="cd-author-info">
                                    <li>Luqman <strong>Amin</strong><br><small>SMP Darul Hikam</small></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- cd-testimonials -->
        </div>
    </div>
</section>

<?php include "partials/section-cta.php";?>


<?php get_footer() ?>
