<?php /* Template Name: Edubox Sekolah Page */ ?>

<?php get_header() ?>

<div class="section-information">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 text-xs-center">
                <h2>
                    Sekolah
                </h2>
                <p>
                    Pinisi Edubox memberikan solusi cerdas untuk melaksanakan ujian online di sekolah anda. <br> Dengan Pinisi Edubox tidak ada lagi masalah dengan koneksi internet yang lambat ataupun server down yang dapat menggangu konsentrasi ujian. Guru bisa fokus untuk meningkatkan kualitas pembelajaran, karena semua pekerjaan administratif sudah dikerjakan oleh Pinisi Edubox
                </p>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>
