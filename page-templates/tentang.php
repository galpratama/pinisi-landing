<?php /* Template Name: Edubox About Page */ ?>

<?php get_header() ?>

<div class="section-information">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1>
                    Tentang Edubox
                </h1>
                <p>
                    Aplikasi pembelajaran berbasis web yang di dalam perangkat cerdas untuk kebutuhan proses belajar mengajar guru di sekolah tanpa harus bergantung kepada akses internet.
                </p>
                <p>
                    Guru di Indonesia selama ini disibukkan dengan pekerjaan administrasi yang menyita waktu seperti memeriksa tugas; menyelenggarakan ujian; rekap nilai dan rapor.
                </p>
                <p>
                    Teknologi bisa membantu guru untuk menyelesaikan permasalahan ini, seperti bermunculan aplikasi berbasis internet seperti Edmodo dan Google Classroom. Tetapi banyak proses implementasi mengalami kegagalan disebabkan buruknya kualitas internet dan konten aplikasi yang tidak sesuai dengan kebutuhan lokal.
                </p>
                <p>
                    Edubox menawarkan solusi modul belajar yang terdiri dari aplikasi ujian , tugas, dan materi dalam jaringan lokal tanpa tergantung kepada internet
                </p>
                <br>
                <h2>
                    Solusi Jaringan Lokal
                </h2>
                <p>
                    Untuk mendapatkan kenyamanan dalam mengakses konten dan aplikasi pembelajaran maka solusi jaringan lokal adalah pilihan utama.
                </p>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>
