<?php /* Template Name: Edubox Pricing Page */ ?>

<?php get_header() ?>

<div class="section-information">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 text-xs-center">
                <h2>
                    Harga
                </h2>
                <p>
                    Berikut ini merupakan daftar harga Pinisi Edubox - Aplikasi pembelajaran berbasis web yang di dalam perangkat cerdas untuk kebutuhan proses belajar mengajar guru di sekolah tanpa harus bergantung kepada akses internet.
                </p>
            </div>
        </div>
    </div>
</div>

<section class="section-pricing-table">
    <div class="container">
        <div class="row">
            <!-- item -->
            <div class="col-md-6 text-xs-center">
                <div class="card card-danger card-pricing">
                    <div class="card-heading">
                        <h3>
                            Edubox <br> <small>Raspberry Pi</small>
                        </h3>
                    </div>
                    <ul class="list-group text-xs-center">
                        <li class="list-group-item"><i class="fa fa-check"></i> ARM A7</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> RAM 1 GB</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Storage 8 GB</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> 80 User</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Edubox Application</li>
                    </ul>
                    <div class="card-footer">
                        <a class="btn btn-lg btn-block btn-danger" href="#">Call <br><small>(0822 1617 3795)</small></a>
                    </div>
                </div>
            </div>
            <!-- /item -->
            <!-- item -->
            <div class="col-md-6 text-xs-center">
                <div class="card card-primary card-pricing">
                    <div class="card-heading">
                        <h3>
                            Edubox <br> <small>UNBK Smart Server</small>
                        </h3>
                    </div>
                    <ul class="list-group text-xs-center">
                        <li class="list-group-item"><i class="fa fa-check"></i> Intel Core i5</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> RAM x GB</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Storage x GB</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> x User</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Edubox Application</li>
                    </ul>
                    <div class="card-footer">
                        <a class="btn btn-lg btn-block btn-primary" href="#">Call <br><small>(0822 1617 3795)</small></a>
                    </div>
                </div>
            </div>
            <!-- /item -->
        </div>
        <br><br><br>
        <div class="row">
            <!-- item -->
            <div class="col-md-4 text-xs-center">
                <div class="card card-warning card-pricing">
                    <div class="card-heading">
                        <h3>
                            Edubox <br> <small>Intel Based</small>
                        </h3>
                    </div>
                    <ul class="list-group text-xs-center">
                        <li class="list-group-item"><i class="fa fa-check"></i> Intel Atom Quad Core</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> RAM 2 GB</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Storage 32 GB</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> 320 User</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Edubox Application</li>
                    </ul>
                    <div class="card-footer">
                        <a class="btn btn-lg btn-block btn-warning" href="#">Call <br><small>(0822 1617 3795)</small></a>
                    </div>
                </div>
            </div>
            <!-- /item -->

            <!-- item -->
            <div class="col-md-4 text-xs-center">
                <div class="card card-success card-pricing">
                    <div class="card-heading">
                        <h3>
                            Edubox <br> <small>Mini Cloud Classroom</small>
                        </h3>
                    </div>
                    <ul class="list-group text-xs-center">
                        <li class="list-group-item"><i class="fa fa-check"></i> Edubox Raspberry Pi</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Mikrotik RB 951Ui 2Hnd</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> 2 Unifi AP</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> Edubox Application</li>
                        <li class="list-group-item">-</li>
                    </ul>
                    <div class="card-footer">
                        <a class="btn btn-lg btn-block btn-success" href="#">Call <br><small>(0822 1617 3795)</small></a>
                    </div>
                </div>
            </div>
            <!-- /item -->

            <!-- item -->
            <div class="col-md-4 text-xs-center">
                <div class="card card-info card-pricing">
                    <div class="card-heading">
                        <h3>
                            Edubox <br> <small>Zenpress</small>
                        </h3>
                    </div>
                    <ul class="list-group text-xs-center">
                        <li class="list-group-item"><i class="fa fa-check"></i> Edubox Applicaton</li>
                        <li class="list-group-item"><i class="fa fa-check"></i> All Zenius Content</li>
                        <li class="list-group-item">-</li>
                        <li class="list-group-item">-</li>
                        <li class="list-group-item">-</li>
                    </ul>
                    <div class="card-footer">
                        <a class="btn btn-lg btn-block btn-info" href="#">Call <br><small>(0822 1617 3795)</small></a>
                    </div>
                </div>
            </div>
            <!-- /item -->
        </div>
        <div class="row">
            <div class="col-md-10 offset-md-1 text-xs-center">
                <br>
                <p>
                    <small><i>*This document is a confidential agreement between the parties involved. Payments should be made promptly and through the specified account information provided by the terms of the transaction.</i></small>
                </p>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>
