<section class="section-cta">
    <div class="container">
        <div class="row text-xs-center">
            <div class="col-md-10 offset-md-1">
                <h1>Pesan Edubox Sekarang</h1>
                <p class="lead">Ujian Online Mudah dan Lancar</p>
                <p>
                    Edubox memastikan kemudahan solusi ujian online tanpa internet untuk sekolah anda.
                    <br>
                    Tunggu apa lagi, segera pesan untuk mendapatkan benefit terbaik dari Edubox!
                </p>
                <p><a class="btn btn-lg btn-success" href="<?php echo get_site_url() . '/edubox/pricing'?>">Lihat Paket</a></p>
            </div>
        </div>
    </div>
</section>
