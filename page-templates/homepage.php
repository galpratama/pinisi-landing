<?php /* Template Name: Pinisi Home Page */ ?>

<?php get_header() ?>

<div class="homepage-hero"
     style="background-image: url('<?php echo get_template_directory_uri() . '/img/pattern-background.png' ?>')">
    <div class="container hero-container text-xs-center">
        <h1 class="hero-title">
            Ujian Online Tanpa Internet
            <br>
            <small>Smarter Education for Smart City</small>
        </h1>
        <div class="hero-button">
            <a href="<?php echo get_site_url() . '/edubox' ?>" class="btn btn-primary btn-lg" href="#">Selengkapnya</a>
        </div>
        <div class="hero-image">
            <img src="<?php echo get_template_directory_uri() . '/img/pinisi-app-preview.png' ?>" alt="">
        </div>
    </div>
</div>

<?php get_footer() ?>
