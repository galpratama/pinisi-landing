<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info pull-left">
						&copy; Pinisi
					</div><!-- .site-info -->

					<div class="site-info pull-right">All Rights Reserved</div>

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page -->

<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script>
	jQuery(document).ready(function($) {
		$('.hero-image').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			adaptiveHeight: true
		});
		$('.cd-testimonials-wrapper').flexslider({
			selector: ".cd-testimonials > li",
			animation: "slide",
			controlNav: true,
			slideshow: false,
			smoothHeight: true,
			start: function() {
				$('.cd-testimonials').children('li').css({
					'opacity': 1,
					'position': 'relative'
				});
			}
		});
	});
</script>

</body>

</html>
